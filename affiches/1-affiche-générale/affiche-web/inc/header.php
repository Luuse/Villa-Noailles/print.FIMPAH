<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style/reset.css" >
    <!-- <link rel="stylesheet" type="text/css" href="style/print.css" media="all"> -->
    <!-- <link rel="stylesheet" type="text/css" href="style/bus.css" media="all"> -->
    <!-- <link rel="stylesheet" type="text/css" href="style/bus_916.css" media="all"> -->
    <!-- <link rel="stylesheet" type="text/css" href="style/print_40x60.css" media="all"> -->
    <!-- <link rel="stylesheet" type="text/css" href="style/print_chasse.css" media="all"> -->
    <!-- <link rel="stylesheet" type="text/css" href="style/print_chasse40x60.css" media="all"> -->
    <!-- <link rel="stylesheet" type="text/css" href="style/print_inauguration.css" media="all"> -->
    <!-- <link rel="stylesheet" type="text/css" href="style/print_inauguration40x60.css" media="all"> -->
    <!-- <link rel="stylesheet" type="text/css" href="style/print_80x120.css" media="all"> -->
    <link rel="stylesheet/less" type="text/less" href="style/print.less" >

    <script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>

    <script src="js/less.min.js" type="text/javascript"></script>
    <script src="js/jquery.arctext.js" type="text/javascript"></script>

    <title>poster</title>
  </head>
  <body>

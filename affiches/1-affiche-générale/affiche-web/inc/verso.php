  <div class="poster verso">
  	<div class="concours accessoire">
  		<div class="flyer fr">
        <div class="text">
          <h4>
          <span class="begin">FESTIVAL</span> </br>INTERNATIONAL</br><span class="end" style="margin-left: 0cm" >D'ACCESSOIRES DE</span></span> MODE</span>
          </h4>
          <h5>deadline<br>le 24 novembre 2017</h5>
          <div class="signature">
            <span class="villa-verso">villa Noailles&nbsp;</span><br>
            centre d'art d'intérêt national
          </div>
          <h6>informations et&nbsp;formulaires sur <sup>www.</sup>villanoailles-hyeres<sup>.com</sup></h6>
        </div>
            <!-- <div class="logo">
              <img src="img/logo/flyerspartnersv.jpg" alt="">
            </div> -->
        </div>
        <div class="flyer en">
          <div class="text">
            <h4>
              <span class="begin">INTERNATIONAL FESTIVAL</br></span>OF<span class="end">ACCESSORIES</span>
            </h4>
            <h5>deadline<br>November the 24<sup>th</sup> 2017</h5>
          <div class="signature">
            <span class="villa-verso">villa Noailles&nbsp;</span><br>
            centre d'art d'intérêt national
          </div>
          <h6>information and entry forms available on <sup>www.</sup>villanoailles-hyeres<sup>.com</sup></h6>
          </div>
            <!-- <div class="logo">
              <img src="img/logo/flyerspartnersv.jpg" alt="">
            </div> -->
        </div>
    </div>
    <div class="concours photo">
      <div class="flyer en">
        <div class="text">
          <h4>
            <span class="begin">INTERNATIONAL</span> </br>FESTIVAL</br><span class="end">OF PHOTOGRAPHY</span>
          </h4>
          <h5>deadline<br>December the 1<sup>st</sup> 2017</h5>
          <div class="signature">
            <span class="villa-verso">villa Noailles&nbsp;</span><br>
            centre d'art d'intérêt national
          </div>
          <h6>information and entry forms available on <sup>www.</sup>villanoailles-hyeres<sup>.com</sup></h6>
        </div>
          <!-- <div class="logo">
            <img src="img/logo/flyerspartnerso.jpg" alt="">
          </div> -->
      </div>
      <div class="flyer fr">
        <div class="text">
          <h4>
            <span class="begin">FESTIVAL</span> </br>INTERNATIONAL</br><span class="end">DE PHOTOGRAPHIE</span>
          </h4>
          <h5>deadline<br>le 1<sup>er</sup> décembre 2017</h5>
          <div class="signature">
            <span class="villa-verso">villa Noailles&nbsp;</span><br>
            centre d'art d'intérêt national
          </div>
          <h6>informations et&nbsp;formulaires sur <sup>www.</sup>villanoailles-hyeres<sup>.com</sup></h6>
        </div>
            <!-- <div class="logo"> -->
              <!-- <img src="img/logo/flyerspartnerso.jpg" alt=""> -->
            <!-- </div> -->
      </div>
    </div>
    <div class="concours mode">
      <div class="flyer fr">
        <div class="text">
          <h4>
            <span class="begin">FESTIVAL</span> </br>INTERNATIONAL</br><span class="end2">DE MODE</span>
          </h4>
          <br>
		  <h5>deadline<br>le 24 novembre 2017</h5>
          <div class="signature">
            <span class="villa-verso">villa Noailles&nbsp;</span><br>
      centre d'art d'intérêt national
          </div>
        <h6>informations et&nbsp;formulaires sur <sup>www.</sup>villanoailles-hyeres<sup>.com</sup></h6>
        </div>
          <!-- <div class="logo">
            <img src="img/logo/flyerspartnersb.jpg" alt="">
          </div> -->
      </div>
      <div class="flyer en">
        <div class="text">
          <h4>
            <span class="begin">INTERNATIONAL</span> </br>FESTIVAL</br><span class="end">OF FASHION</span>
          </h4>
          <h5>deadline<br>November the 24<sup>th</sup> 2017</h5>
          <div class="signature">
            <span class="villa-verso">villa Noailles&nbsp;</span><br>
            centre d'art d'intérêt national
          </div>
          <h6>information and entry forms available on <sup>www.</sup>villanoailles-hyeres<sup>.com</sup></h6>
        </div>
            <!-- <div class="logo">
              <img src="img/logo/flyerspartnersb.jpg" alt="">
            </div> -->
      </div>
    </div>
  </div>

 <div class="poster">
<div class="fond">
  <img src="img/FONDS/08.pdf" alt="" />
</div>

<!-- T I T R E S -->
    <div class="stroke-titre">
      <!-- <h1 class="fr"> -->
      <!--     <div class="vert villa" > -->
      <!--     villa Noailles </div>Architecture, Mode, Design, Architecture d’intérieur, Photographie  -->
      <!--   </h1> -->
        <h1 class="fr">
          <div class="vert">
<span class="sup">Appels</span> à <br><span style="margin-left: 6cm" class="sup2">can</span>didatures<br><span class="scl_0-6" >33</span><sup>e</sup> <span class="inf" >festival</span> interna<span class="inf2">tional</span><br>à <span class="scl_0-6">Hyères</span> <span class="scl_0-6" >26</span> - <span class="scl_0-6">30</span> <span style="margin-left: 6cm" class="dot.bak">avril</span> <span class="scl_0-6 inf4" >2018</span>
          </div>
        </h1>
      <h1 class="en">
        <div class="orange" >
        <span class="sup">Call f</span>or<br><span class="sup">appli</span>cation <span class="scl_0-6">33</span><sup>rd</sup><br><span class="inf">international</span> <span class="scl_0-8">festival</span> <br>in hyères <span class="scl_0-6">26</span><sup>th</sup> - <span class="scl_0-6">30</span><sup>th</sup> April <span class="scl_0-6 inf4">2018</span>
        </div>

      </h1>
    </div>

<!-- S T R O K E    T I T R E -->
    <div class="titre">
      <!-- <h1 class="fr"> -->
      <!--     <div class="vert villa" > -->
      <!--     villa Noailles </div>Architecture, Mode, Design, Architecture d’intérieur, Photographie  -->
      <!--   </h1> -->
        <h1 class="fr">
          <div class="vert">
<span class="sup">Appels</span> à <br><span style="margin-left: 6cm" class="sup2">can</span>didatures<br><span class="scl_0-6" >33</span><sup>e</sup> <span class="inf" >festival</span> interna<span class="inf2">tional</span><br>à <span class="scl_0-6">Hyères</span> <span class="scl_0-6" >26</span> - <span class="scl_0-6">30</span> <span style="margin-left: 6cm" class="dot.bak">avril</span> <span class="scl_0-6 inf4" >2018</span>
          </div>
        </h1>
      <h1 class="en">
        <div class="orange" >
<span class="sup">Call f</span>or<br><span class="sup">appli</span>cation <span class="scl_0-6">33</span><sup>rd</sup><br><span class="inf">international</span> <span class="scl_0-8">festival</span> <br>in hyères <span class="scl_0-6">26</span><sup>th</sup> - <span class="scl_0-6">30</span><sup>th</sup> April <span class="scl_0-6 inf4">2018</span>
        </div>

      </h1>
    </div>

    <div class="sous-titre" >
      <div class="hyeres">
        <span class="sup3">
          Communauté d'Agglomération Toulon Provence Méditerranée
        </span>
      </div>
      <div class="villa">
        <span style="margin-left: 2cm" class="inf">villa </span><br><span class="sup">Noailles</span><span class="inf5"> -Hyères</span>
      </div>
      <div class="date">
        26.04 - 30.04
      </div>
    </div>
    <div class="concours accessoire">
      <div class="flyer fr">
        <div class="text">
          <h3>
            &laquo;Je déclare ouvert les appels à candidature du 33<sup>e</sup><span> festival international d’Accessoires de Hyères.&raquo;</span>
          </h3>
          <h4>
            Bertrand Guillon,<br>21 place vendôme, Paris</br> samedi 29 septembre, 15:32.
          </h4>
        </div>
      <h5>villa Noailles, hyères</h5>
      </div>
      <div class="flyer en">
        <div class="text">
          <h3>
            &ldquo;I hereby declare open the call for application for the 33<sup>th</sup> International Festival of Accessories of Hyères.&rdquo;
          </h3>
          <h4>
            Bertrand Guillon,<br>21 place vendôme, Paris</br> Saturday september 29<sup>th</sup>, 15:32.
          </h4>
        </div>
        <h5>villa Noailles, hyères</h5>
      </div>
    </div>
    <div class="concours photo">
      <div class="flyer en">
        <div class="text">
          <h3>
            &ldquo;I hereby declare open the call for application for the 33<sup>rd</sup> <span>International Festival of Photography of Hyères.&rdquo;</span>
          </h3>
          <h4>
            Charles Freger,<br> 106 rue de la pompe, Paris</br> Sunday October 1<sup>st</sup>, 17:27.
          </h4>
        </div>
        <h5>villanoailles-hyeres.com</h5>
      </div>
      <div class="flyer fr">
        <div class="text">
          <h3>
            &laquo;Je déclare ouvert les appels à candidature du 33<sup>e</sup> festival international de Photographie de Hyères.&raquo;
          </h3>
          <h4>
            Charles Freger,<br> 106 rue de la pompe, Paris</br> dimanche 1 octobre, 17:27.
          </h4>
        </div>
        <h5>villanoailles-hyeres.com</h5>
      </div>
    </div>
    <div class="concours mode">
      <div class="flyer fr">
        <div class="text">
          <h3>
            &laquo;Je déclare ouvert les appels à candidature du 33<sup>e</sup> festival international de Mode de Hyères.&raquo;
          </h3>
          <h4>
            Pierre Hardy,<br> 3 avenue du général Eisenhower, Paris</br> lundi 2 octobre, 11:34.
          </h4>
        </div>
        <h5>villa Noailles, hyères</h5>
      </div>
      <div class="flyer en">
        <div class="text">
          <h3>
            &ldquo;I hereby declare open the call for application for the 33<sup>rd</sup> <span>International Festival of Fashion of Hyères.&rdquo;</span>
          </h3>
          <h4>
            Pierre Hardy,<br> 3 avenue du général Eisenhower, Paris</br> Monday october 2<sup>nd</sup>, 11:34
          </h4>
        </div>
        <h5>villa Noailles, hyères</h5>
      </div>
    </div>
  </div>

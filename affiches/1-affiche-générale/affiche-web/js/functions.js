function rand(start, end){

	return Math.round(Math.floor(Math.random() * end) + start);

}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function isOdd(num) {
  return num % 2;
}

function zoomPoster(range, content, fond){

  var inputOnLoad = range.val();
  document.addEventListener('input', function(e) {
    var value = e.target.value/100;
    console.log(value);
    if(e.target.className == 'rangeValue'){
      content.css({'transform': 'scale('+value+')'});
      fond.css({'transform': 'scale('+value+')'});
    }
  })
}

function hideGrids(grids, button){
  button.click(function(){
    let theClass = $(this).attr('class').split(' ')[1];
    theGrid = grids.find('.'+theClass);
    if ($(this).hasClass('active')) {
      $(this).removeClass('active')
      theGrid.show();
    } else {
      $(this).addClass('active')
      theGrid.hide();
    }

  })
}

#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os.path
import json
from PIL import Image

if not os.path.isfile(sys.argv[1]):
    print("ERREUR:" + sys.argv[1] + " n'est pas un fichier.")
    exit(1)
im = Image.open(sys.argv[1], 'r')
namefile = os.path.splitext(sys.argv[1])[0].replace('input/', '')
print(namefile)
im.load()
im = im.convert('RGBA')
pix_val = list(im.getdata())
print(pix_val)
print('---------------')
dataPixels = []
count = 0

for i in range(im.height):
    print(i)
    for u in range(im.width):

        cible = count
        hexad = '#%02x%02x%02x' % (pix_val[cible][0], pix_val[cible][1], pix_val[cible][2])
        dataPixels += [{'pix': (hexad, u, i)}]
        print('->' + str(cible) + '-> ' + str(cible) + '->' + hexad)
        count = count + 1

print('---------------')
data = {
    'name': namefile,
    'height': im.height,
    'width': im.width,
    'pixels': dataPixels
}

with open('json/' + namefile + '.json', 'w') as f:
    json.dump(data, f, indent=2)

exit(0)


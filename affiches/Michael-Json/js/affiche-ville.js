// // REGLAGE AFFICHE 320X240
MichaelJson({
//bosquet red-haut
font: 'Hershey-Noailles-herbier',
glyphs: ['8','a','f','i','m','1','n','q','r'],
findColor: '#ff0000',
glyphColor: ['#F25E23','#4D20E8'],
freq: [1,0.5],
rotate: -180,
translateX: 2,
incTransX: 0,
translateY: -10,
incTransY: 0,
scan: [0, 70],
incStyle: [
  ['font-size', 2.5, 'px', 0.005],
  // ['opacity', 1, '', -0.005]
]
});
MichaelJson({
//bosquet black-haut
font: 'Hershey-Noailles-herbier',
glyphs: ['2','3','5','7','q','o','r'],
findColor: '#000000',
glyphColor: ['#15B965','#4D20E8'],
freq: [1,0.5],
rotate: 180,
translateX: 0,
incTransX: 0,
translateY: -12,
incTransY: 0,
scan: [0, 40],
incStyle: [
  ['font-size', 2.5, 'px', 0.001],
 // ['opacity', 1, '', -0.0005]
]
});
MichaelJson({
//bosquet black-middle
font: 'Hershey-Noailles-herbier',
glyphs: ['2','3','5','7','q','o','r'],
findColor: '#000000',
glyphColor: ['#15B965','#4D20E8'],
freq: [1,0.5],
rotate: -180,
translateX: -10,
incTransX: 0,
translateY: -2,
incTransY: 0,
scan: [41, 90],
incStyle: [
  ['font-size', 3, 'px', 0.005],
 // ['opacity', 1, '', -0.0005]
]
});
// BOSQUET BLUE ALL
MichaelJson({
//bosquet blue-haut
font: 'Hershey-Noailles-herbier',
glyphs: ['4','5','6','9','h','j','p','3','7'],
findColor: '#0000ff',
glyphColor: ['#F25E23','#15B965'],
freq: [1,0.5],
rotate: -180,
translateX: 2,
incTransX: 0,
translateY: -20,
incTransY: 0,
scan: [0, 40],
incStyle: [
  ['font-size', 3, 'px', 0.005],
  // ['font-size', 5, 'px', 0.0007],
  // ['opacity', 1, '', 0]
]
});
MichaelJson({
//bosquet blue-midle
font: 'Hershey-Noailles-herbier',
glyphs: ['4','5','6','9','h','j','p','3','7'],
findColor: '#0000ff',
glyphColor: ['#F25E23','#15B965'],
freq: [1,0.5],
rotate: 360,
translateX: -2,
incTransX: 0,
translateY: 2,
incTransY: 0,
scan: [41, 70],
incStyle: [
  ['font-size', 2, 'px', 0.005],
  // ['font-size', 5, 'px', 0.0007],
  // ['opacity', 1, '', 0]
]
});
MichaelJson({
//bosquet blue-midle
font: 'Hershey-Noailles-herbier',
glyphs: ['4','5','6','9','h','j','p','3','7'],
findColor: '#0000ff',
glyphColor: ['#F25E23','#15B965'],
freq: [1,0.5],
rotate: 360,
translateX: -22,
incTransX: 0,
translateY: -22,
incTransY: 0,
scan: [71, 90],
incStyle: [
  ['font-size', 2, 'px', 0.005],
  // ['font-size', 5, 'px', 0.0007],
  // ['opacity', 1, '', 0]
]
});
//BOSQUET RED ALL


MichaelJson({
//bosquet blue-bas
font: 'Hershey-Noailles-herbier',
glyphs: ['4','5','6','9','h','j','p','3','7'],
findColor: '#0000ff',
glyphColor: ['#F25E23','#15B965'],
freq: [1,0.5],
rotate: 360,
translateX: -2,
incTransX: 0,
translateY: -2,
incTransY: 0,
scan: [111, 149],
incStyle: [
  ['font-size', 3, 'px', 0.005],
  // ['font-size', 5, 'px', 0.0007],
  // ['opacity', 1, '', 0]
]
});
// MichaelJson({
// //bosquet white
// font: 'Hershey-Noailles-herbier',
// glyphs: ['4','5','6','9','h','j','p','3','7'],
// findColor: '#ffffff',
// glyphColor: ['white'],
// freq: [1,0.5],
// rotate: 360,
// translateX: 0,
// incTransX: 0,
// translateY: -6,
// incTransY: 0,
// scan: [80, 90],
// incStyle: [
//   ['font-size', 3, 'px', 0.005],
//   // ['font-size', 5, 'px', 0.0007],
//   // ['opacity', 1, '', 0]
// ]
// });
MichaelJson({
//bosquet red-bas
font: 'Hershey-Noailles-herbier',
glyphs: ['8','a','f','i','m','1','n','q','r'],
findColor: '#ff0000',
glyphColor: ['#F25E23','#4D20E8'],
freq: [1,0.5],
rotate: 360,
translateX: 0,
incTransX: 0,
translateY: -10,
incTransY: 0,
scan: [71,90],
incStyle: [
  ['font-size', 3, 'px', 0.002],
  // ['opacity', 1, '', -0.005]
]
});

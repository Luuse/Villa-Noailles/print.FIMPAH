MichaelJson({
    //noir
    font: 'Hershey-Noailles-herbier-stroke0-5',
    glyphs: ['1', '2', '3', '4', '5','6', '7', '8', '9',],
    findColor: '#000000',
    glyphColor: ['#f15f24'],
    freq: [1, 1],
    rotate: 360,
    translateX: 40,
    incTransX: 0,
    translateY: 0,
    incTransY: 20,
    scan: [0, 90],
    incStyle: [
 ['font-size', 0.04, 'px', 0.03],
  // ['font-size', 5, 'px', 0.0007],
  //['opacity', 5, '', 0.004]
]
});

MichaelJson({
    //noir
    font: 'Hershey-Noailles-herbier-stroke0-5',
    glyphs: ['i', 'o', '4', 'q', '3'],
    findColor: '#000000',
    glyphColor: ['#624319','#505951'],
    freq: [1, 1],
    rotate: 0,
    translateX: 30,
    incTransX: 50,
    translateY: 0,
    incTransY: 0,
    scan: [0, 90],
    incStyle: [
  //['font-size', 1, 'px', 0.3],
  // ['font-size', 5, 'px', 0.0007],
  //['opacity', 5, '', 0.004]
]
});

// REGLAGES AFFICHE GENERALE --- PAS TOUCHER!!!! ---
// BOSQUET BLUE ALL (vert+ orange)
MichaelJson({
//bosquet blue-haut
font: 'Hershey-Noailles-herbier',
glyphs: ['4','5','6','9','h','j','p','3','7'],
findColor: '#0000ff',
glyphColor: ['#FF334D','#15B965'],
freq: [1,0.5],
rotate: 360,
translateX: -10,
incTransX: 0,
translateY: -25,
incTransY: 0,
scan: [0, 70],
incStyle: [
  ['font-size', 3, 'px', 0.007],
  // ['font-size', 5, 'px', 0.0007],
  // ['opacity', 1, '', 0]
]
});
MichaelJson({
//bosquet blue-midle
font: 'Hershey-Noailles-herbier',
glyphs: ['4','5','6','9','h','j','p','3','7'],
findColor: '#0000ff',
glyphColor: ['#FF334D','#15B965'],
freq: [1,0.5],
rotate: 360,
translateX: -2,
incTransX: 0,
translateY: -2,
incTransY: 0,
scan: [71, 140],
incStyle: [
  ['font-size', 3, 'px', 0.005],
  // ['font-size', 5, 'px', 0.0007],
  // ['opacity', 1, '', 0]
]
});

// FIN BOSQUET BLUE ALL
//BOSQUET RED ALL (orange + bleu)
    MichaelJson({
    //bosquet red-haut
    font: 'Hershey-Noailles-herbier',
    glyphs: ['8','a','f','i','m','1','n','q','r'],
    findColor: '#ff0000',
    glyphColor: ['#FF334D','#4D20E8'],
    freq: [1,0.5],
    rotate: -180,
    translateX: -2,
    incTransX: 0,
    translateY: -10,
    incTransY: 0,
    scan: [0, 57],
    incStyle: [
      ['font-size', 4, 'px', 0.01],
      // ['opacity', 1, '', -0.005]
    ]
    });

    MichaelJson({
    //bosquet red-bas
    font: 'Hershey-Noailles-herbier',
    glyphs: ['8','a','f','i','m','1','n','q','r'],
    findColor: '#ff0000',
    glyphColor: ['#FF334D','#4D20E8'],
    freq: [1,0.5],
    rotate: -180,
    translateX: -20,
    incTransX: 0,
    translateY: -20,
    incTransY: 0,
    scan: [111, 177],
    incStyle: [
      ['font-size', 3, 'px', 0.006],
      // ['opacity', 1, '', -0.005]
    ]
    });
// FIN BOSQUET RED ALL
// BOSQUET BLACK ALL ( vert + bleu)
MichaelJson({
//bosquet black-haut
font: 'Hershey-Noailles-herbier',
glyphs: ['2','3','5','7','q','o','r'],
findColor: '#000000',
glyphColor: ['#15B965','#4D20E8'],
freq: [1,0.5],
rotate: -90,
translateX: 0,
incTransX: 0,
translateY: -2,
incTransY: 0,
scan: [0, 60],
incStyle: [
  ['font-size', 3, 'px', 0.001],
 // ['opacity', 1, '', -0.0005]
]
});
// MichaelJson({
// //bosquet black-middle
// font: 'Hershey-Noailles-herbier',
// glyphs: ['2','3','5','7','q','o','r'],
// findColor: '#000000',
// glyphColor: ['#15B965','#4D20E8'],
// freq: [1,0.5],
// rotate: 180,
// translateX: -25,
// incTransX: 0,
// translateY: 2,
// incTransY: 0,
// scan: [61, 120],
// incStyle: [
//   ['font-size', 3, 'px', 0.003],
//  // ['opacity', 1, '', -0.0005]
// ]
// });
MichaelJson({
//bosquet red-middle
font: 'Hershey-Noailles-herbier',
glyphs: ['8','a','f','i','m','1','n','q','r'],
findColor: '#ff0000',
// glyphColor: ['red'],
glyphColor: ['#FF334D','#4D20E8'],

freq: [1,0.5],
rotate: -180,
translateX: -2,
incTransX: 0,
translateY: -2,
incTransY: 0,
scan: [58, 110],
incStyle: [
  ['font-size', 2, 'px', 0.006],
  // ['opacity', 1, '', -0.005]
]
});
MichaelJson({
//bosquet black-bas
font: 'Hershey-Noailles-herbier',
glyphs: ['2','3','5','7','q','o','r'],
findColor: '#000000',
glyphColor: ['#15B965','#4D20E8'],
freq: [1,0.5],
rotate: 90,
translateX: 10,
incTransX: 0,
translateY: -20,
incTransY: 0,
scan: [121, 177],
incStyle: [
  ['font-size', 3, 'px', 0.001],
 // ['opacity', 1, '', -0.0005]
]
});
MichaelJson({
//bosquet blue-bas
font: 'Hershey-Noailles-herbier',
glyphs: ['4','5','6','9','h','j','p','3','7'],
findColor: '#0000ff',
glyphColor: ['#FF334D','#15B965'],
freq: [1,0.5],
rotate: 25,
translateX: -2,
incTransX: 0,
translateY: -2,
incTransY: 0,
scan: [141, 177],
incStyle: [
  ['font-size', 4, 'px', 0.0009],
  // ['font-size', 5, 'px', 0.0007],
  // ['opacity', 1, '', 0]
]
});
MichaelJson({
//bosquet white-bas
font: 'Hershey-Noailles-herbier',
glyphs: ['4','5','6','9','h','j','p','3','7'],
findColor: '#00ffff',
glyphColor: ['white'],
freq: [1,0.5],
rotate: 180,
translateX: 0,
incTransX: 0,
translateY: -10,
incTransY: 0,
scan: [121, 177],
incStyle: [
  ['font-size', 3, 'px', 0.001],
 // ['opacity', 1, '', -0.0005]
]
});

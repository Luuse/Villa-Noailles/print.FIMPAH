# Michael Json
Luuse 2017

![anime](screen/final-color-bd.gif)
![anime](screen/final-bd.gif)
![anime](screen/change-size.png)
![anime](screen/final-scan.png)

## Requirement

  * Python
    ⤷ Modules : PIL, os, json, sys
    
## Générer le json

Il faut placer l'image (jpg, jpeg, png) d'entrée dans le répertoire `input/`.
Ensuite il suffit de lancer la commande suivante.

` $ python Michael-Json.py input/image.jpg `

`Michael-Json.py` va convertir les pixels de l'images pour en faire un fichier
`.json` dans le repertoire `json/`.

## Visualiser le json

Il faut lancer l'`index.html` suivit d'un `hash` avec le nom de votre json.
Par exemple: `index.html#votreImage`.

## Modifier la fonction de lecture du json

La lecture du Json se fait en javacript.

```
MichaelJson({
    findColor: '#000000', // l'entrée de la couleur à chercher
    glyphColor: [ 'red', 'blue'], // les couleurs d'affichage
    freq: [1,2], // la fréquence des glyphs (exemple: [1, 2] -> un glyph sur deux est chargé
    rotate: 180, // un random de 0 à 180deg glyph
    translateX: 10, // le glyph se donne 10 pixels en x de liberté
    incTransX: 1, // à chaque nouveau pixel le translateX augmente de 1px
    translateY: 10, // le glyph se donne 10 pixels en y de liberté
    scan: [min, max], // 'all' pour l'entièreté du scan
    incTransY: 1, // à chaque nouveau pixel le translateY augmente de 1px
    incStyle: [ [ 'cssFunction', 'startValue', 'incValue' ] ],
});
```

## License

[GNU General Public License (GPL)](https://www.gnu.org/licenses/gpl-3.0.en.html)



# FIMPAH PRINT
Luuse 2018
.
├── affiches
│   ├── 1-affiche-générale
│   ├── 2-affiche-accessoire
│   ├── 3-affiche-mode
│   ├── 4-affiche-photo
│   ├── 5-affiches-expos
│   ├── affiche-ville
│   ├── Michael-Json
│   ├── Michael-Json-back
│   └── ressources
├── carnet-jury
│   ├── carnet-accessoire-mode
│   ├── carnet-mode
│   ├── carnet-photo
│   ├── couv-carnets-jurys
│   ├── couv-carnets-jurys.zip
│   ├── formsSVG
│   ├── formsSVG.zip
│   ├── livret jury séléction.docx
│   ├── logoVN-en.svg
│   ├── Michael-Json
│   ├── pdf2svg
│   ├── pdf2svg.zip
│   ├── ref-pantones.txt
│   └── text.svg
├── festival-mode-logo
│   ├── logo-save-the-date-a-utiliser
│   └── recherchelogo
├── README.md
├── reseaux-sociaux
│   └── HEADER-FIMPAH.png
└── save-the-date
    ├── cartels
    ├── carte-save-the-date
    ├── invitation-mail
    ├── pub-magazine
    └── save.svg

27 directories, 10 files
